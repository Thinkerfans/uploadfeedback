#include <string.h>
#include <jni.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <android/log.h>
#include <unistd.h>
#include <sys/inotify.h>
#include <dirent.h>

//LOG宏定义
#define BH_DEBUG "jni dubug :"

#ifdef BH_DEBUG
#define LOG_INFO(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define LOG_DEBUG(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define LOG_WARN(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#define LOG_ERROR(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)
#else
#define LOG_INFO(...)
#define LOG_DEBUG(...)
#define LOG_WARN(...)
#define LOG_ERROR(...)
#endif

#define TAG "bh_common"

/* 内全局变量begin */
static char c_TAG[] = "bh_common";
static jboolean isCopy = JNI_TRUE;
static int first = 0;

void list_process()
{
	LOG_INFO("list_process enter");
	FILE *fp;
	pid_t pid;
	pid_t cpid = getpid();
	int fd;
	struct dirent *d;
	char exefile[FILENAME_MAX];
	char realfile[FILENAME_MAX];
	char statusfile[FILENAME_MAX];
	char line[256], *p;

	DIR *dir = opendir("/proc");
    if (dir == NULL) {
        return;
    }
	while((d = readdir(dir)) != NULL)
	{
		pid = strtoul(d->d_name, NULL, 0);
        if (pid <= 0 || pid == cpid) { // avoid current
            continue;
        }
		snprintf(exefile, sizeof(exefile), "/proc/%s/exe", d->d_name);

		memset(realfile, 0, sizeof(realfile));
		if (readlink(exefile, realfile, FILENAME_MAX) == -1) {
			continue;
		}
		LOG_INFO("id:%s file:%s", d->d_name, realfile);
		snprintf(statusfile, sizeof(statusfile), "/proc/%s/status", d->d_name);
		
		fp = fopen(statusfile, "r");
		
        if (fp == NULL) {
			LOG_INFO("statusfile %s open failed", statusfile);
            continue;
        }
		int ppid = -1;
		while((p = fgets(line, sizeof(line) - 1, fp)) != NULL){
			if(strncmp(p, "PPid", 4) == 0){
				p = strchr(p, ':');
				ppid = atoi(++p);
				break;
			}
		}
        fclose(fp);
		LOG_INFO("ppid:%d\n", ppid);
		if(ppid == 1){
			if(kill(pid, SIGKILL) == 0)
			{
				LOG_INFO("kill %d done", pid);
			}
			else
			{
				LOG_INFO("kill %d failed", pid);
			}
		}
		
	}
	closedir(dir);
}

int is_remote()
{
	char line[256], *p;
	FILE *fp = fopen("/proc/self/status", "r");
    if (fp == NULL) {
		LOG_INFO("self status open failed");
        return 0;
    }
	while((p = fgets(line, sizeof(line) - 1, fp)) != NULL){
		if(strncmp(p, "Name", 4) == 0 && strstr(p, ":remote") != NULL){
			LOG_INFO("remote process. quit");
			return 1;
		}
	}
	return 0;
}


void watch_uninstall(JNIEnv* env, jobject thiz, jstring component, jstring pkg, jstring userSerial, jstring url)
{
    //初始化log
    LOG_DEBUG("enter watch");
	if(first != 0){
		LOG_DEBUG("already watching");
		return;
	}

	if(is_remote()){
		return;
	}
	first = 1;
	
	list_process();
	
    //fork子进程，以执行轮询任务
    pid_t pid = fork();
    LOG_DEBUG("pid=%d",pid);


    if (pid < 0)
    {
        //出错log
        LOG_ERROR("fork failed !!!");
    }
    else if (pid == 0)
    {

        LOG_DEBUG("pid=%d",pid);
        int fileDescriptor = inotify_init();
        if (fileDescriptor < 0)
        {
            LOG_DEBUG("inotify_init failed !!!");
            exit(1);
        }

        int watchDescriptor;
		char app_path[1024] = "/data/data/";
		char lib_path[1024] = {0};
		strcat(app_path, (*env)->GetStringUTFChars(env, pkg, &isCopy));
		
		strcpy(lib_path, app_path);
//		strcat(lib_path, "/lib");
		LOG_DEBUG("path=%s",lib_path);
	//	LOG_DEBUG("serial=%s",(*env)->GetStringUTFChars(env, userSerial, &isCopy));
        watchDescriptor = inotify_add_watch(fileDescriptor, lib_path, IN_DELETE);
        LOG_DEBUG("inotify_add_watch re=%d",watchDescriptor);
        if (watchDescriptor < 0)
        {
            LOG_DEBUG("inotify_add_watch failed !!!");
            exit(1);
        }

        //分配缓存，以便读取event，缓存大小=一个struct inotify_event的大小，这样一次处理一个event
        void *p_buf = malloc(sizeof(struct inotify_event));
        if (p_buf == NULL)
        {
            LOG_DEBUG("malloc failed !!!");
            exit(1);
        }
        //开始监听
        LOG_DEBUG("start observer");
        size_t readBytes = read(fileDescriptor, p_buf, sizeof(struct inotify_event));
        LOG_DEBUG(" read size %d",readBytes);
        //read会阻塞进程，走到这里说明收到目录被删除的事件，注销监听器
        free(p_buf);
        inotify_rm_watch(fileDescriptor, IN_DELETE);
        LOG_DEBUG(" inotify_rm_watch ");
	//	usleep(5000);
		int exist = access(app_path, F_OK);
		LOG_DEBUG(" exist  %d",exist);
		if (exist == 0)
		{
			LOG_DEBUG("path:%s exist. not uninstall", app_path);
		//	exit(0);
		}
		
        //目录不存在log
        LOG_DEBUG("uninstalled ");

		
        if (userSerial == NULL)
		{
			LOG_DEBUG("user Serial is null");
		    // 执行命令am start -a android.intent.action.VIEW -d $(url)
             execlp("am", "am", "start",
			 "-a", "android.intent.action.VIEW",
			 "-d", (*env)->GetStringUTFChars(env, url, &isCopy),
			 "-n", (*env)->GetStringUTFChars(env, component, &isCopy),
			 (char *)NULL);
         }
         else
         {
 		 	LOG_DEBUG("user Serial is not null url=%s,serial=%s,component=%s",
 		 			(*env)->GetStringUTFChars(env, url, &isCopy),
 		 			(*env)->GetStringUTFChars(env, userSerial, &isCopy),
 		 			(*env)->GetStringUTFChars(env, component, &isCopy)
 		 			);
			int re= execlp("am", "am", "start","--user",
			(*env)->GetStringUTFChars(env, userSerial, &isCopy),
			"-a", "android.intent.action.VIEW",
			"-d", (*env)->GetStringUTFChars(env, url, &isCopy),
			"-n", (*env)->GetStringUTFChars(env, component, &isCopy),
			(char *)NULL);
         }

    }
    else
    {
        //父进程直接退出，使子进程被init进程领养，以避免子进程僵死

    }
}

static JNINativeMethod gMethods[] = {
    {"watchUninstall", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", (void*)watch_uninstall}
};

static int registerNativeMethods(JNIEnv* env
        , const char* className
        , JNINativeMethod* gMethods, int numMethods) {
    jclass clazz;
    clazz = (*env)->FindClass(env, className);

    if (clazz == NULL) {
        return JNI_FALSE;
    }

    if ((*env)->RegisterNatives(env, clazz, gMethods, numMethods) < 0) {
        return JNI_FALSE;
    }

    return JNI_TRUE;
}

static int registerNatives(JNIEnv* env) {
    const char* kClassName = "com/android/uploadfeedback/UpLoadFeedBackLib";//指定要注册的类
    return registerNativeMethods(env, kClassName, gMethods,
            sizeof(gMethods) / sizeof(gMethods[0]));
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved) {
    JNIEnv* env = NULL;
    jint result = -1;

    if ((*vm)->GetEnv(vm, (void**) &env, JNI_VERSION_1_4) != JNI_OK) {
        return -1;
    }
    assert(env != NULL);
    if (!registerNatives(env)) {//注册
        return -1;
    }
    //成功
    result = JNI_VERSION_1_4;

    return result;
}

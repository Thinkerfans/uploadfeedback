package com.example.uploadfeedback;

import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.uploadfeedback.UpLoadFeedBackLib;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		String component = UpLoadFeedBackLib.getAvailableBrowser(this);
		UpLoadFeedBackLib.watchUninstall(component, this.getPackageName(),
				UpLoadFeedBackLib.getUserSerial(this),
				UpLoadFeedBackLib.getUninstallSurveryUrl(this));
	}

	public void onClick(View v) {
		execOpenBrowser();
	}

	private void execOpenBrowser() {

		int v = android.os.Build.VERSION.SDK_INT;
		String[] cmd = null;
		if (v < android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {

			cmd = new String[] { "am", "start", "-a",
					"android.intent.action.VIEW", "-d", "http://www.baidu.com",
					"-n",
					"com.android.browser/com.android.browser.BrowserActivity", };
		} else {
			cmd = new String[] { "am", "start", "--user", "0", "-a",
					"android.intent.action.VIEW", "-d", "http://www.baidu.com",
					"-n",
					"com.android.browser/com.android.browser.BrowserActivity", };
		}
		
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			Log.e("test---", (p != null) ? p.toString() : "result is null");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

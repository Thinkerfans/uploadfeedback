package com.android.uploadfeedback;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.util.HashSet;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

public class UpLoadFeedBackLib {
	private static final String TAG = "UpLoadFeedBackLib";

	private static String[] sBrowsers = { "com.UCMobile", "com.tencent.mtt", "com.oupeng.mini.android",
			"com.qihoo.browser", "com.baidu.browser.apps", "org.mozilla.firefox", "com.dolphin.browser.xf",
			"com.android.chrome", "com.android.browser" };

	static HashSet<String> sBrowserSet;
	static {
		sBrowserSet = new HashSet<String>();
		for (String s : sBrowsers) {
			sBrowserSet.add(s);
		}
	}

	static {
		System.loadLibrary("uploadfeedback");
	}

	public static native void watchUninstall(String component, String pkg, String userSerial, String surveryUrl);

	public static String getAvailableBrowser(Context c) {
		PackageManager pm = c.getPackageManager();
		List<PackageInfo> list = pm.getInstalledPackages(0);
		if (list == null) {
			return null;
		}
		for (PackageInfo pi : list) {

			if (!sBrowserSet.contains(pi.packageName)) {
				continue;
			}

			Intent it = pm.getLaunchIntentForPackage(pi.packageName);

			if (it != null) {
				ComponentName component = it.getComponent();
				String s = component.getPackageName() + "/" + component.getClassName();
				Log.e(TAG, s);
				return s;
			}
		}
		return null;
	}

	// 由于targetSdkVersion低于17，只能通过反射获取
	public static String getUserSerial(Context c) {
		Object userManager = c.getSystemService("user");
		if (userManager == null) {
			Log.e(TAG, "userManager not exsit !!!");
			return null;
		}

		try {
			Method myUserHandleMethod = android.os.Process.class.getMethod("myUserHandle", (Class<?>[]) null);
			Object myUserHandle = myUserHandleMethod.invoke(android.os.Process.class, (Object[]) null);

			Method getSerialNumberForUser = userManager.getClass().getMethod("getSerialNumberForUser",
					myUserHandle.getClass());
			long userSerial = (Long) getSerialNumberForUser.invoke(userManager, myUserHandle);
			
			Log.e(TAG, "userManager notuserSerial"+userSerial);

			return String.valueOf(userSerial);
		} catch (Exception e){
			e.printStackTrace();
		}

		return null;
	}
	
	public static String getUninstallSurveryUrl(Context c) {	
		return "http://www.baidu.com";
	}

	private static String md5(String str) {
		if (str == null) {
			return null;
		}
		return md5(str, "utf-8");
	}

	private static String md5(String str, String encodingType) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		try {
			md5.update(str.getBytes(encodingType));
		} catch (UnsupportedEncodingException e) {
			md5.update(str.getBytes());
		}

		byte[] md5Bytes = md5.digest();

		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < md5Bytes.length; i++) {
			int val = ((int) md5Bytes[i]) & 0xff;
			if (val < 16) {
				hexValue.append("0");
			}
			hexValue.append(Integer.toHexString(val));
		}
		return hexValue.toString();
	}
}